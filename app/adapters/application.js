import RESTAdapter from 'ember-data/adapters/rest';
import Ember from 'ember';
export default RESTAdapter.extend({
  host: 'https://api.github.com',
  session: Ember.inject.service(),
  buildURL() {
    let url = this._super(...arguments);
    return url.replace("%2F", "/");
  },

  headers: Ember.computed('session.token', function() {
    return {
      "Authorization": `token ${this.get('session.token')}`
    };
  })
});
